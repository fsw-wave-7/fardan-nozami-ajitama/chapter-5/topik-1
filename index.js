const chalk = require("chalk");
const { getUser, writeFile } = require("./user.js");
const userjson = require("./user.json");

console.log("halo saya sedang belajar Nodejs");
console.log(chalk.blue("Saya sedang menggunakan third Party"));
console.log(chalk.green("ini warna hijau dengan menggunakan chalk"));

const user = getUser("nozami", 28);
console.log(user);

writeFile(user);
console.log(userjson);
